# -*- coding: utf-8 -*-
from openprocurement.api.views.award_complaint import TenderAwardComplaintResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Award Complaints',
            collection_path='/tenders/{tender_id}/awards/{award_id}/complaints',
            path='/tenders/{tender_id}/awards/{award_id}/complaints/{complaint_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP award complaints")
class TenderUPAwardComplaintResource(TenderAwardComplaintResource):
    pass
