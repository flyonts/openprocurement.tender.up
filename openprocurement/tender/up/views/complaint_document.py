# -*- coding: utf-8 -*-
from openprocurement.api.views.complaint_document import TenderComplaintDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Complaint Documents',
            collection_path='/tenders/{tender_id}/complaints/{complaint_id}/documents',
            path='/tenders/{tender_id}/complaints/{complaint_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP complaint documents")
class TenderUPComplaintDocumentResource(TenderComplaintDocumentResource):
    pass
