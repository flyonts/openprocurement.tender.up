# -*- coding: utf-8 -*-
from openprocurement.api.views.contract import TenderAwardContractResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Contracts',
            collection_path='/tenders/{tender_id}/contracts',
            path='/tenders/{tender_id}/contracts/{contract_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP contracts")
class TenderUPAwardContractResource(TenderAwardContractResource):
    pass
