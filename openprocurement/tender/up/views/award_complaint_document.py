# -*- coding: utf-8 -*-
from openprocurement.api.views.award_complaint_document import TenderAwardComplaintDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Award Complaint Documents',
            collection_path='/tenders/{tender_id}/awards/{award_id}/complaints/{complaint_id}/documents',
            path='/tenders/{tender_id}/awards/{award_id}/complaints/{complaint_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP award complaint documents")
class TenderUPAwardComplaintDocumentResource(TenderAwardComplaintDocumentResource):
    pass
