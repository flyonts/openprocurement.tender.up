# -*- coding: utf-8 -*-
from openprocurement.api.views.cancellation_document import TenderCancellationDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Cancellation Documents',
            collection_path='/tenders/{tender_id}/cancellations/{cancellation_id}/documents',
            path='/tenders/{tender_id}/cancellations/{cancellation_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP cancellation documents")
class TenderUPCancellationDocumentResource(TenderCancellationDocumentResource):
    pass
