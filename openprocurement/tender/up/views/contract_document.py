# -*- coding: utf-8 -*-
from openprocurement.api.views.contract_document import TenderAwardContractDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Contract Documents',
            collection_path='/tenders/{tender_id}/contracts/{contract_id}/documents',
            path='/tenders/{tender_id}/contracts/{contract_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP contract documents")
class TenderUPAwardContractDocumentResource(TenderAwardContractDocumentResource):
    pass
