# -*- coding: utf-8 -*-
from openprocurement.api.views.bid import TenderBidResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Bids',
            collection_path='/tenders/{tender_id}/bids',
            path='/tenders/{tender_id}/bids/{bid_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP bids")
class TenderUPBidResource(TenderBidResource):
    pass
