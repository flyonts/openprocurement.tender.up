# -*- coding: utf-8 -*-
from openprocurement.api.views.question import TenderQuestionResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Questions',
            collection_path='/tenders/{tender_id}/questions',
            path='/tenders/{tender_id}/questions/{question_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP questions")
class TenderUPQuestionResource(TenderQuestionResource):
    pass
