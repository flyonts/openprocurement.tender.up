# -*- coding: utf-8 -*-
from openprocurement.api.views.complaint import TenderComplaintResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Complaints',
            collection_path='/tenders/{tender_id}/complaints',
            path='/tenders/{tender_id}/complaints/{complaint_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP complaints")
class TenderUPComplaintResource(TenderComplaintResource):
    pass
