# -*- coding: utf-8 -*-
from openprocurement.api.views.bid_document import TenderBidDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Bid Documents',
            collection_path='/tenders/{tender_id}/bids/{bid_id}/documents',
            path='/tenders/{tender_id}/bids/{bid_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP bidder documents")
class TenderUPBidDocumentResource(TenderBidDocumentResource):
    pass
