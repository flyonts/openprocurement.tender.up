# -*- coding: utf-8 -*-
from openprocurement.api.views.tender_document import TenderDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender UP Documents',
            collection_path='/tenders/{tender_id}/documents',
            path='/tenders/{tender_id}/documents/{document_id}',
            procurementMethodType='belowThresholdUP',
            description="Tender UP related binary files (PDFs, etc.)")
class TenderUPDocumentResource(TenderDocumentResource):
    pass
